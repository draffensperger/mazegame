/*
 * TrashCustomTask.java
 *
 * Created on November 30, 2001, 3:48 PM
 */

// IMPORTANT! You may need to mount ant.jar before this class will
// compile. So mount the JAR modules/ext/ant.jar (NOT modules/ant.jar)
// from your IDE installation directory in your Filesystems before
// continuing to ensure that it is in your classpath.

import org.apache.tools.ant.*;
import org.apache.tools.ant.types.*;

/**
 *
 * @author  dave
 * @version 
 */
public class TrashCustomTask extends Task {

    /* For a simple option:
    private boolean opt;
    public void setOpt (boolean b) {
        opt = b;
    }
    // <customtask opt="true"/>
    */

    /* For a simple property based on a string:
    private String myprop;
    public void setMyprop (String s) {
        myprop = s;
    }
    // <customtask myprop="some text here"/>
    */

    /* For a simple property based on a file:
    private File myfile;
    public void setMyfile (File f) {
        // Note: f will automatically be absolute (resolved from project basedir).
        myfile = f;
    }
    // <customtask myfile="foo.txt"/>
    */

    /* Custom nested elements:
    public static class Nestme {
        String val; // accessible from execute()
        public void setVal (String s) {
            val = s;
        }
    }
    private List nestmes = new LinkedList (); // List<Nestme>
    public Nestme createNestme () {
        Nestme n = new Nestme ();
        nestmes.add (n);
        return n;
    }
    // Or:
    public void addNestme (Nestme n) {
        nestmes.add (n);
    }
    // <customtask>
    //   <nestme val="something"/>
    // </customtask>
    */

    /* To add embedded filesets:
    private List filesets = new LinkedList (); // List<FileSet>
    public void addFileset (FileSet fs) {
        filesets.add (fs);
    }
    // <customtask>
    //   <fileset dir="foo">
    //     <include name="*.txt"/>
    //   </fileset>
    // </customtask>
    // In execute() you can do:
    Iterator it = filesets.iterator ();
    while (it.hasNext ()) {
        FileSet fs = (FileSet) it.next ();
        DirectoryScanner ds = fs.getDirectoryScanner (project);
        File basedir = ds.getBasedir ();
        String[] files = ds.getIncludedFiles ();
        // process them...
    }
    */

    /* For nested text:
    private StringBuffer text;
    public void addText (String t) {
        t = t.trim ();
        if (text == null) {
            text = new StringBuffer (t);
        } else {
            text.append (t);
        }
    }
    // <customtask>
    //   Some text...
    // </customtask>
    */

    /* Some sort of path (like classpath or similar):
    private Path path;
    public void setPath (Path p) {
        if (path == null) {
            path = p;
        } else {
            path.append (p);
        }
    }
    public Path createPath () {
        if (path == null) {
            path = new Path (project);
        }
        return path.createPath ();
    }
    public void setPathRef (Reference r) {
        createPath ().setRefid (r);
    }
    // <customtask path="foo:bar"/>
    // <customtask>
    //   <path>
    //     <pathelement location="foo"/>
    //   </path>
    // </customtask>
    // Etc.
    */

    /* One of a fixed set of choices:
    public static class FooBieBletch extends EnumeratedAttribute {
        public String[] getValues () {
            return new String[] { "foo", "bie", "bletch" };
        }
    }
    private String mode = "foo";
    public void setMode (FooBieBletch m) {
        mode = m.getValue ();
    }
    // <customtask mode="bletch"/>
    */

    public void execute () throws BuildException {
        // What the task actually does:
        // WRITEME

        // To log something:
        // log ("Some message");
        // log ("Serious message", Project.MSG_WARN);
        // log ("Minor message", Project.MSG_VERBOSE);

        // To signal an error:
        // throw new BuildException ("Problem", location);
        // throw new BuildException (someThrowable, location);
        // throw new BuildException ("Problem", someThrowable, location);

        // You can call other tasks too:
        // Zip zip = (Zip) project.createTask ("zip");
        // zip.setZipfile (zipFile);
        // FileSet fs = new FileSet ();
        // fs.setDir (baseDir);
        // zip.addFileset (fs);
        // zip.init ();
        // zip.setLocation (location);
        // zip.execute ();
    }

}
