package draff.lang.reflect;

import java.lang.reflect.*;
import java.util.StringTokenizer;
import java.util.Enumeration;
import java.util.Vector;

/**
 * A class to decode expressions in much the same way a Java compiler decodes.
 */
public class ExpressionDecoder
{	
	/**
	 * Constucts an object by calling the correct constuctor given a list of constructor parameter
	 * and a fully qualified class name (the preceding "new " is optional). e.g.:
	 * Object obj = ExpressionDecoder.constuctorObject("new java.lang.Thread(new ThreadGroup(\"1\"), (String)null)");
	 * Here are some differences it has to a normal Java code constructor call:
	 *		no semicolon is needed at the end
	 *		the preceding new is not needed
	 *		null parameters should always be casted
	 *		all number parameters besides int must either have a decimal to indicate double,
	 *			a preceeding f or F to indicate float, l or L to indicate double, or be casted
	 *			to the correct type
	 *		currently arrays are not supported
	 *		unnecessary parenthesis should not be included
	 *		expressions should be full evaulated, (i.e. don't include expressions like 1 < 2 or 3 - 4 etc.)
	 * @param constructorCall The String giving the parameters for the constructor call in a Java-syntax-like format
	 * @return The object constructed from the constructor call.
	 * @exception ClassNotFoundExcepiton if the class in the call does not exist. (remember to use fully qualified names)
	 * @exception NoSuchMethodException if the spesified constructor does not exist.
	 * @exception SecurityExcepiton if access to constructor information is denied.
	 * @exception IllegalAccessException if the underlying constructor is inaccessible. 
	 * @exception IllegalArgumentException if the number of actual and formal parameters differ, or if an unwrapping conversion fails. 
	 * @exception InstantiationException if the class that declares the underlying constructor represents an abstract class.
	 * @exception InvocationTargetException if the underlying constructor throws an exception.
	 */
	public static Object constructObject(String constructorCall) throws ClassNotFoundException,
					NoSuchMethodException, SecurityException, IllegalAccessException,
					IllegalArgumentException, InstantiationException, InvocationTargetException {
		String trimConstructorCall = constructorCall.trim();
		
		if (trimConstructorCall.indexOf("new ") != -1) {
			trimConstructorCall = trimConstructorCall.substring(trimConstructorCall.indexOf("new ") + 3).trim();
		}		
		
		ArgumentsSeparater armuments = new ArgumentsSeparater(trimConstructorCall);
			
		Class[] parametersInDefinition = new Class[armuments.getArgumentCount()];
		Object[] parametersInCall = new Object[armuments.getArgumentCount()];
			
		for (int paramIndex = 0; paramIndex < parametersInCall.length; paramIndex++) {
			String parameter = armuments.nextArgument();
				
			parametersInDefinition[paramIndex] = determineType(parameter);
			parametersInCall[paramIndex] = determineValue(parameter);
		}
			
		return Class.forName(new StringTokenizer(trimConstructorCall, "(",
												 false).nextToken()).getDeclaredConstructor(
				parametersInDefinition).newInstance(parametersInCall);
	}

	
	/**
	 * Determines the values of the spesified expression in a Java-syntax-like manner.
	 * Same limitations as the parameters of the constructObject method.
	 * @see #constuctObject(String)
	 * @param expression The expression to be decoded.
	 * @return The values of the expression. An object if it was a constructor call, or a wrapper object if it was primitive.
	 * @exception ClassNotFoundExcepiton if type an expression (the parameter or an exclosed parameter to a constructor)
	 * @exception NoSuchMethodException if the expression object and the spesified constructor does not exist.
	 * @exception SecurityExcepiton if the expression object and access to constructor information is denied.
	 * @exception IllegalAccessException if the expression object and and underlying constructor is inaccessible. 
	 * @exception IllegalArgumentException if the expression object and the number of actual and formal parameters of its constructor differ, or if an unwrapping conversion fails. 
	 * @exception InstantiationException if the expression object it class that declares the underlying constructor represents an abstract class.
	 * @exception InvocationTargetException if the expression object and its underlying constructor throws an exception.
	 */
	public static Object determineValue(String expression) throws ClassNotFoundException,
				InstantiationException, NoSuchMethodException, IllegalAccessException,
				InvocationTargetException {
		String trimExpression = expression.trim();
		Class expressionType = determineType(expression);
		
		try {
			if (trimExpression.substring(trimExpression.indexOf(")") + 1).equals("null")) {			
				return null;
			}
		} catch (StringIndexOutOfBoundsException err) {
			if (trimExpression.equals("null")) {
				return null;
			}	
		}
		if (expressionType == (new String()).getClass())
		{
			// the expression with the " at the begining and end removed.
			return new String(trimExpression.substring(trimExpression.indexOf("\"") + 1,
													   trimExpression.lastIndexOf("\"")));
		} else if (expressionType.isPrimitive()) {
			String castingRemovedExpression; 
			
			if (trimExpression.lastIndexOf(")") != -1) {				
				castingRemovedExpression = trimExpression.substring(trimExpression.indexOf(")") + 1);
			} else {
				castingRemovedExpression = trimExpression;
			}
			
			if (expressionType == Boolean.TYPE) {
				return new Boolean(castingRemovedExpression);
			} else if (expressionType == Byte.TYPE) {
				return new Byte(castingRemovedExpression);
			} else if (expressionType == Short.TYPE) {
				return new Short(castingRemovedExpression);
			} else if (expressionType == Integer.TYPE) {
				return new Integer(castingRemovedExpression);
			} else if (expressionType == Long.TYPE) {
				if (Character.toUpperCase(castingRemovedExpression.charAt(castingRemovedExpression.length() - 1)) == 'L') {
					return new Long(castingRemovedExpression.substring(0, castingRemovedExpression.length() - 1));
				}
				return new Long(castingRemovedExpression);
			} else if (expressionType == Float.TYPE) {
				return new Float(castingRemovedExpression);
			} else if (expressionType == Double.TYPE) {
				return new Double(castingRemovedExpression);
			} else if (expressionType == Character.TYPE) {
				return castingRemovedExpression.substring(1, 2);
			}
		} else if (!expressionType.isArray()) {
			return constructObject(trimExpression);
		}
		
		throw new IllegalArgumentException("Unknown value for expression: " + expression);
	}
	
	/**
	 * Determines the type of an expression.
	 * @param expression The expression who's type is to be determined.
	 * @return The class the expression represents or the correct primitive type wrapper class TYPE field.
	 * @exception ClassNotFoundException if the expression does not represent a known class or it is not properly formed.
	 */
	public static Class determineType(String expression) throws ClassNotFoundException {
		String trimExpression = expression.trim();
		
		if (trimExpression.indexOf("(") == 0) {
			return getClassForName(trimExpression.substring(trimExpression.indexOf("(") + 1, 
														  trimExpression.indexOf(")")));
		}
		try {
			if (trimExpression.substring(trimExpression.indexOf(")")).equals("null")) {			
				return new Object().getClass();
			}
		} catch (StringIndexOutOfBoundsException err) {
			if (trimExpression.equals("null")) {
				return new Object().getClass();
			}	
		} 
		if (trimExpression.indexOf("\"") == 0) {
			return (new String()).getClass();
		} else if (trimExpression.indexOf("'") == 0) {
			return Character.TYPE;
		} else if (trimExpression.equals("true") || trimExpression.equals("false")) {
			return Boolean.TYPE;
		} else if (Character.isDigit(trimExpression.charAt(0))) {
			if (Character.toUpperCase(trimExpression.charAt(trimExpression.length() - 1)) == 'F') {
				return Float.TYPE;
			} else if (trimExpression.indexOf(".") != -1) {
				return Double.TYPE;
			} else {
				if (Character.toUpperCase(trimExpression.charAt(trimExpression.length() - 1)) == 'L') {
					return Long.TYPE;
				} else {
					return Integer.TYPE;
				}
			}
		} else {
			String expressionWithNoNew = trimExpression;
			
			if (trimExpression.startsWith("new ")) {
				expressionWithNoNew = trimExpression.substring(3).trim();
			}
			
			StringBuffer className = new StringBuffer();
				
			int charIndex = 0;
			try {
				while (Character.isJavaIdentifierPart(expressionWithNoNew.charAt(charIndex)) ||
					   expressionWithNoNew.charAt(charIndex) == '.') {
					className.append(expressionWithNoNew.charAt(charIndex));
					charIndex++;
				}
			} catch (StringIndexOutOfBoundsException err) { 
				err.printStackTrace();
			}
			
			return getClassForName(className.toString());
		}
	}
	
	/**
	 * Exactly the same as Class.forName(Sring) except that it understands the primitive type class names (e.g. int, boolean, etc.).
	 * @param name The fully qualified name of the desired class.
	 * @return The class occiated with the spesified name.
	 * @exception ClassNotFoundException if the class could not be found.
	 */
	public static Class getClassForName(String name) throws ClassNotFoundException {
		String trimName = name.trim();
		
		if (trimName.equals("byte")) {
			return Byte.TYPE;
		} else if (trimName.equals("short")) {
			return Short.TYPE;
		} else if (trimName.equals("int")) {
			return Integer.TYPE;
		} else if (trimName.equals("long")) {
			return Long.TYPE;
		} else if (trimName.equals("float")) {
			return Float.TYPE;
		} else if (trimName.equals("double")) {
			return Double.TYPE;
		} else if (trimName.equals("boolean")) {
			return Boolean.TYPE;
		} else if (trimName.equals("char")) {
			return Character.TYPE;
		} else {
			return Class.forName(name);
		}
	}
}

class ArgumentsSeparater implements Enumeration {
	private String[] argumentsArray;
	private int argumentIndex = -1; // to make code for nextElement() simpler
	
	public ArgumentsSeparater(String constructorCall) {
		String trimConstructorCall = constructorCall.trim();
		
		StringTokenizer argumentsTokenizer = new StringTokenizer(
				trimConstructorCall.substring(trimConstructorCall.indexOf("(") + 1,
				trimConstructorCall.lastIndexOf(")")), ",", false);
		
		Vector arguments = new Vector();
		String trimArgument;
		for (int paramIndex = 0; argumentsTokenizer.hasMoreTokens(); paramIndex++) {
			trimArgument = argumentsTokenizer.nextToken().trim();
			
			if (areParenthesisBalanced(trimArgument)) {
				arguments.addElement(trimArgument);
			} else {				
				int parenthesisBalance = getParenthesisBalance(trimArgument);
				
				if (parenthesisBalance > 0) {
					throw new IllegalArgumentException("Too many ')'s in call: " + constructorCall);
				} else {
					StringBuffer subArguments = new StringBuffer();
					subArguments.append(trimArgument);	
					
					String subArgument;
					do {
						// Commas are removed by the StringTokienizer argumentsTokenizer, return tokens
						// should still be false so that balanced parenthesis arguments will not have
						// trailing commas.
						subArguments.append(',');	
						
						subArgument = argumentsTokenizer.nextToken();
						subArguments.append(subArgument);
						parenthesisBalance += getParenthesisBalance(subArgument);
					} while(parenthesisBalance != 0);
					
					arguments.addElement(subArguments.toString());
				}
			}
		}
		
		Object[] uncastedArguments = new String[arguments.size()];
		arguments.copyInto(uncastedArguments);		
		argumentsArray = (String[])uncastedArguments;
	}
	
	public String nextArgument() {
		return (String)nextElement();
	}
	
	public int getArgumentCount() {
		return argumentsArray.length;
	}
	
	public Object nextElement() {
		argumentIndex++;
		return argumentsArray[argumentIndex];
	}
	
	public boolean hasMoreElements() {
		return argumentIndex < argumentsArray.length - 1;
	}
	
	private boolean areParenthesisBalanced(String testString) {
		return  getParenthesisBalance(testString) == 0;
	}
	
	// negative indicated too many '(', positive indicates too many ')'
	private int getParenthesisBalance(String testString) {
		return numberOf(')', testString) - numberOf('(', testString);
	}
	
	private int numberOf(char testChar, String searchString) {
		char[] searchCharArray = searchString.toCharArray();
		
		int numberOfChars = 0;
		for (int searchCharIndex = 0; searchCharIndex < searchCharArray.length; searchCharIndex++) {
			if (testChar == searchCharArray[searchCharIndex]) {
				numberOfChars++;
			}
		}
		
		return numberOfChars;
	}
}