package draff.io;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.BufferedReader;

public class StringMaker {
   public static String readStringFromInputStream (InputStream whatToReadFrom)
      throws IOException {

      return readStringFromReader (new InputStreamReader (whatToReadFrom));
   }

   public static String readStringFromReader (Reader whatToReadFrom)
      throws IOException {

      BufferedReader stringReader = new BufferedReader (whatToReadFrom);
      StringBuffer currentString = new StringBuffer ();
      String currentLine = stringReader.readLine ();

      while (currentLine != null) {
         currentString.append (currentLine);
         currentLine = stringReader.readLine ();
      }

      return currentString.toString ();
   }
}
