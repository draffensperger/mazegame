package draff.io;

import java.io.Writer;
import java.io.BufferedWriter;
import java.io.OutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;

public class StringSaver {
   public static void writeStringToWriter (Writer destination,
      String text) throws IOException {

      BufferedWriter stringWriter = new BufferedWriter (destination);

      stringWriter.write (text, 0, text.length ());
   }

   public static void writeStringToOutputStream (OutputStream destination,
      String text) throws IOException {

      writeStringToWriter (new OutputStreamWriter (destination), text);
   }
}
