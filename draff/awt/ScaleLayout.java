package draff.awt;

import java.awt.*;
import java.util.Hashtable;

public class ScaleLayout implements LayoutManager2, java.io.Serializable {
	/** @see ScaleLayout(Dimension) */
	private Dimension startSize;
	/** The components that will be layed out.*/
	private Hashtable componentsInLayout;
	
	/**
	 * Creats a ScaleLayout.
	 * @param startSize The size that should be used as a frame of reference for the current size of a container
	 */
	public ScaleLayout(Dimension startSize)
	{
		this.componentsInLayout =  new Hashtable();
		this.startSize = startSize;
	}
	
	/**
	 * Lays out a container.
	 * It moves and resizes to scale every component that belongs to the parent that was added with 
	 * addLayoutComponent(Component).
	 * @param parent The container to lay out.
	 * @see ScaleLayout
	 * @see addLayoutComponent(Component)
	 */
	public synchronized void layoutContainer(Container parent)
	{				
		double widthChangeInParent =
						(((double)parent.getSize().width) / ((double)startSize.width));
		double heightChangeInParent =
						(((double)parent.getSize().height) / ((double)startSize.height));
		
	 	for(int componentIndex = 0; componentIndex < parent.getComponents().length; componentIndex++)
		{			
			if(componentsInLayout.containsKey(parent.getComponent(componentIndex)))
			{
				Rectangle startBounds =
						(Rectangle)componentsInLayout.get(parent.getComponent(componentIndex));
				
				parent.getComponent(componentIndex).setLocation((int)(startBounds.x * widthChangeInParent),
						(int)(startBounds.y * heightChangeInParent));
				parent.getComponent(componentIndex).setSize((int)(startBounds.width * widthChangeInParent),
						(int)(startBounds.height * heightChangeInParent));
			}
		}	
	}
	
	public void addLayoutComponent(Component comp, Object constraints) {
		if (constraints instanceof Rectangle && constraints != null) {
			componentsInLayout.put(comp, (Rectangle)constraints);
		} else {
			componentsInLayout.put(comp, comp.getBounds());
		}
	}
	
	public void addLayoutComponent(String name, Component comp) {
		addLayoutComponent(comp, null);
	}
	
	public void removeLayoutComponent(Component comp) {
		componentsInLayout.remove(comp);
	}
	
	/**
	 * @see LayoutManager#minimumLayoutSize(Container)
	 * @return new Dimension(1, 1)
	 */
	public Dimension minimumLayoutSize(Container parent)
	{
		return new Dimension(1, 1);
	}
		
	/**
	 * @see LayoutManager#preferredLayoutSize(Container)
	 * return The minimumLayoutSize().
	 */
	public Dimension preferredLayoutSize(Container parent)
	{
		return minimumLayoutSize(parent);
	}
	
	public void invalidateLayout(Container parent) {}
	
	public float getLayoutAlignmentX(Container parent) {
		return 0.5F;
	}
											  
	public float getLayoutAlignmentY(Container parent) {
		return 0.5F;
	}
											  	
	public Dimension maximumLayoutSize(Container parent) {
		return minimumLayoutSize(parent);
	}
	
	public void setStartBounds(Component c, Rectangle newStartBounds) {
		componentsInLayout.put(c, newStartBounds);
	}
	
	public void moveComponent(Component c, Point location) {
		Rectangle oldStartBounds = (Rectangle)componentsInLayout.get(c);
		componentsInLayout.put(c, new Rectangle(location.x, location.y, oldStartBounds.width, 
												oldStartBounds.height));
	}
	
	public String toString() {
		return "ScaleLayout[" + startSize + "]";
	}
}