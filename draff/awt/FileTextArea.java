package draff.awt;

import java.awt.TextArea;
import java.io.*;
import draff.io.*;

public class FileTextArea extends TextArea {
   private File source;

   public FileTextArea (File source) {
      this.source = source;
   }

   public void setFile (File source) {
      this.source = source;
   }

   public File getFile () {
      return source;
   }

   public void load () throws IOException {
      setText (StringMaker.readStringFromReader (new FileReader (getFile ())));
   }

   public void save () throws IOException {
      StringSaver.writeStringToWriter (new FileWriter (getFile ()), getText ());
   }
}
