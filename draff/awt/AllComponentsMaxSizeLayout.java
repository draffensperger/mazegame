package draff.awt;

import java.awt.*;

public class AllComponentsMaxSizeLayout implements LayoutManager, java.io.Serializable {
	public void layoutContainer(Container parent) {
		for (int componentIndex = 0; componentIndex < parent.getComponents().length; componentIndex++) {
			parent.getComponent(componentIndex).setSize(parent.getSize());
			parent.getComponent(componentIndex).setLocation(0, 0);
		}
	}

	public Dimension minimumLayoutSize(Container parent) {
		return parent.getSize();
	}
	
	public Dimension preferredLayoutSize(Container parent) {
		return parent.getSize();
	}
	
	public void addLayoutComponent(String name, Component c) {}	
	public void removeLayoutComponent(Component c) {}
}
