package draff.awt;

import java.awt.LayoutManager;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;

/**
 * A layout manager that will make one component act as a background and thus occupy its whole
 * parent and in other respects is like having null for a layout manager.
 *
 * @author David Adam Raffensperger
 * @version 1.0 14 Feb 1999
 */
public class BackgroundComponentLayout implements LayoutManager
{
	/** The component that will fill the full space of its parent. */
	private Component background;

	/** Constructs a BackgroundComponentLayout
	 * @param background The component that will occupy the full space of its parent.
	 */
	public BackgroundComponentLayout(Component background)
	{
		setBackground(background);
	}

	/**
	 * @return The component that will occupy the full space of its parent.
	 */
	public Component getBackground()
	{
		return background;
	}

	/**
	 * @see getBackground()
	 */
	public void setBackground(Component background)
	{
		this.background = background;
	}

	public void layoutContainer(Container parent)
	{
		background.setLocation(0, 0);
		background.setSize(parent.getSize());
	}

	public Dimension minimumLayoutSize(Container parent)
	{
		return background.getMinimumSize();
	}

	public Dimension preferredLayoutSize(Container parent)
	{
		return background.getPreferredSize();
	}

	public void addLayoutComponent(String name, Component comp) {}
	public void removeLayoutComponent(Component comp) {}
}