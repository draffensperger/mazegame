package draff.awt;

public class LightWeightPanel extends java.awt.Container {
	private static int numberInExistance = 0;
	
	public LightWeightPanel() {		
		setName("LightWeightPanel" + numberInExistance);
		numberInExistance++;
	}
	
	public String toString() {
		return super.toString() + getLayout();
	}
}
