package draff.awt.event;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Window;

public class ShowWindowActionListener implements ActionListener
{
	private Window whatToShow;

	public ShowWindowActionListener(Window whatToShow)
	{
		this.whatToShow = whatToShow;
	}

	public void actionPerformed(ActionEvent event)
	{
		whatToShow.show();
	}
}
