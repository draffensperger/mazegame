package draff.awt.event;

import draff.awt.MediaSupplier;
import draff.awt.TextAreaFrame;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.*;
import java.io.*;

public class DisplayTextFileActionListener implements ActionListener
{
	private String fileName;
	private Font displayFont;

	public DisplayTextFileActionListener(String fileName, Font displayFont) {
		this.fileName = fileName;
		this.displayFont = displayFont;
	}

	public void actionPerformed(ActionEvent evt)
	{
		TextAreaFrame displayFrame = new TextAreaFrame(fileName, readTextFromFile(fileName));
		displayFrame.setFont(displayFont);

		displayFrame.pack();
		displayFrame.setVisible(true);
	}

	private static String readTextFromFile(String fileName)
	{
		try
		{
			BufferedReader reader = 
					new BufferedReader(new InputStreamReader(MediaSupplier.getInputStream(fileName)));
			StringBuffer fileContent = new StringBuffer();
			String currentLine = reader.readLine();

			while(currentLine != null)
			{
				fileContent.append(currentLine + "\n");
				currentLine = reader.readLine();	
			}

			return fileContent.toString();
		}
		catch(Exception err)
		{
			err.printStackTrace();
			return null;
		}
	}
}