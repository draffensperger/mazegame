
package draff.awt.event;

import java.awt.event.*;
import java.awt.*;

public class HandOverEventActionListener implements ActionListener {
	private MenuComponent realMenuComponentHandler = null;
	private Button realButtonHandler = null;
	
	public HandOverEventActionListener(MenuComponent realMenuComponentHandler) {
		this.realMenuComponentHandler = realMenuComponentHandler;
	}
	
	public HandOverEventActionListener(Button realButtonHandler) {
		this.realButtonHandler = realButtonHandler;
	}
	
	public void actionPerformed(ActionEvent evt) {
		if (realButtonHandler != null) {
			realButtonHandler.dispatchEvent(evt);
		} else if (realMenuComponentHandler != null) {
			realMenuComponentHandler.dispatchEvent(evt);
		}
	}
}
