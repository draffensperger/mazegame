package draff.awt.event;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class WindowCloseWindowListener extends WindowAdapter
{
	public void windowClosing(WindowEvent evt)
	{
		evt.getWindow().setVisible(false);
		evt.getWindow().dispose();
	}
}