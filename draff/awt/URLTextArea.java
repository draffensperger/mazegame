package draff.awt;

import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.io.*;
import java.net.*;

public class URLTextArea extends TextArea
{
   private URL source;

   public URLTextArea (URL source) throws IOException
   {
      this.source = source;
      reLoad ();

      //addKeyListener (new TabSizeKeyAdapter (3));
   }

   public void reLoad () throws IOException
   {
      setText (getStringFromURL (getSource ()));
   }

   public void save () throws IOException
   {
      URLConnection connection = getSource ().openConnection ();

      connection.connect ();

      BufferedWriter sourceWriter;

      try
         {
         sourceWriter = new BufferedWriter (
            new OutputStreamWriter (connection.getOutputStream ()));
         }
      catch (UnknownServiceException err)
         {
         if (getSource ().getProtocol ().equals ("file"))
            {
            sourceWriter = new BufferedWriter (
               new OutputStreamWriter (new FileOutputStream (getSource ().getFile ())));
            }
         else
            {
               throw err;
            }
         }

      sourceWriter.write (getText (), 0, getText ().length ());
      sourceWriter.close ();
   }

   public void print (Graphics g)
   {
   }

   public void setSource (URL source)
   {
      this.source = source;
   }

   public URL getSource ()
   {
      return source;
   }

   private static String getStringFromURL (URL whereToGetString)
      throws IOException
   {
      if (whereToGetString.getContent () instanceof String)
         {
         return whereToGetString.getContent ().toString ();
         }

      StringBuffer contents = new StringBuffer ();
      BufferedReader contentReader = new BufferedReader (
         new InputStreamReader (whereToGetString.openStream ()));

      String currentLine = contentReader.readLine ();

      while (currentLine != null)
         {
         contents.append (currentLine);
         contents.append ("\n");
         currentLine = contentReader.readLine ();
         }

       return contents.toString ();
   }
}

class TabSizeKeyAdapter extends KeyAdapter
{
   private String fakeTab;

   public TabSizeKeyAdapter (int spacesInTab)
   {
      StringBuffer fakeTabBuffer = new StringBuffer ();

      for (int i = 1; i <= spacesInTab; i++)
      {
         fakeTabBuffer.append (' ');
      }

      fakeTab = fakeTabBuffer.toString ();
   }

   public void keyPressed (KeyEvent evt)
   {
      TextArea text;

      if (!(evt.getSource () instanceof TextArea))
      {
         return;
      }

      text = (TextArea) evt.getSource ();

      if (evt.getKeyCode () == KeyEvent.VK_TAB)
         {
         if (text.getCaretPosition () >
            getCurrentLine (text.getText (), text.getCaretPosition ()) + 2)
            {
            text.setCaretPosition (
               text.getCaretPosition () -
               getCurrentLine (text.getText (), text.getCaretPosition ())
               - 2 + fakeTab.length ());
            }

         text.replaceRange (
            fakeTab,
            text.getCaretPosition (),
            text.getCaretPosition ());

         // Needed because of weirdosity in TextArea.replaceRange ()

         evt.consume ();
         }
   }

   private int getCurrentLine (String str, int pos)
   {
      StringTokenizer tokenizer = new StringTokenizer (str, "\n");
      int i = 0;
      int prevLength = 0;
                 
      while (tokenizer.hasMoreTokens ())
         {
         i++;

         String token = tokenizer.nextToken ();

         if (pos > prevLength && pos < prevLength + token.length ())
            {
            return i;
            }

         prevLength += token.length ();
         }

      return 1;
   }
}
