package imagedisplayer;

import draff.awt.*;
import java.awt.*;

public class ImageDisplayer
{
	public static void main(String[] args)
	{
		String imageFileName = args[0];
		
		Frame displayFrame = new Frame(imageFileName);
		ImageCanvas displayer = new ImageCanvas(imageFileName);
		//displayer.loadImage();
		displayer.setSize(displayer.getPreferredSize());
		displayFrame.add(displayer);
		displayFrame.setSize(displayer.getPreferredSize());
		displayFrame.setVisible(true);
		
		System.out.println("Image = " + displayer.getImage());
		Animator paint = new Animator(displayFrame);
		paint.start();
	}	
}

class Animator extends Thread {
	Component whatToPaint;
	
	public Animator(Component c) {
		whatToPaint = c;
	}
		
	public void run() {
		while (true) {
			try {
				Thread.sleep(100);
			} catch (Exception err) {}
			whatToPaint.repaint();
		}
	}
}