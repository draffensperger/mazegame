package mazecomponents;

import java.awt.event.*;
import mazegame.*;
import java.awt.Point;

public class KeyRoomItem extends RoomItem implements Runnable {
	private InventoryItem key;
	
	public KeyRoomItem(String imageFile, Point location, String keyImageFile, String keyName) {
		super (imageFile, location);
		key = new InventoryItem(keyImageFile, keyName);
	}
	
	public boolean isActionDefinedFor(InventoryItem actor) {
		return actor instanceof HandInventoryItem;
	}
	
	public void mousePressed(MouseEvent evt) {
		getMaze().prepareForChange();
		getMaze().getQuester().addInventoryItem(key);
		getMaze().getRooms()[getMaze().getQuester().getRoomLocatedIn()].removeRoomItem(this);
		new Thread(this).start();
	}

	// In order for the key inventory item to when the key is pick up, the update must
	// take place in antother thread.
	public void run() {
		getMaze().update();
		key.repaint();
	}
}
