package mazegame;

import java.awt.*;
import java.util.*;
import draff.awt.LightWeightPanel;

public class InventoryDisplay extends MazeViewerPanel {
	private Hashtable itemHolderPanels;
	
	public InventoryDisplay(Maze maze) {
		super(maze);
		
		setLayout(new FlowLayout(FlowLayout.LEFT));
	}

	public void mazeStateChanged(Maze previousState) {
		removeAll();
		
		Enumeration inventroy =  getMaze().getQuester().getInventory();
		while (inventroy.hasMoreElements()) {
			add((InventoryItem)inventroy.nextElement());
		}
		
		// This statement is necessary because otherwise the components will group at the top left.
		validate();
		repaint();
	}
}