package mazegame;

import java.awt.*;
import java.util.Observer;
import java.util.Observable;

public abstract class MazeViewerPanel extends Container implements Observer {
	private Maze maze = null;
	private int mazeViewerPanelCount;

	public MazeViewerPanel(Maze maze) {
		setName(getClass().getName() + mazeViewerPanelCount);
		mazeViewerPanelCount++;
		setVisible(true);
		
		if (maze != null) {
			setMaze(maze);
		}
	}

	public Maze getMaze() {
		return maze;
	}

	public void setMaze(Maze maze) {
		if (this.maze != null) {
			this.maze.deleteObserver(this);
		}
		
		this.maze = maze;
		this.maze.addObserver(this);
	}
	
	public abstract void mazeStateChanged(Maze previousState);

	public void update(Observable o, Object arg) {
		mazeStateChanged((Maze)arg);
	}
}
