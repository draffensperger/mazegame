package mazegame;

import java.awt.*;
import java.awt.event.*;
import java.applet.*;
import draff.awt.*;
import draff.awt.event.*;
import java.util.zip.*;
import java.io.*;

public class MazeApplet extends Applet {
	private MazeContainer game;

	public static void main (String args[]) {		
		Frame displayFrame = new Frame();
		displayFrame.addWindowListener(new SystemExitWindowListener());
		displayFrame.setIconImage(MediaSupplier.getImage("images/mazegameicon.jpg"));
		displayFrame.setTitle("MazeGame");
		displayFrame.setSize(Toolkit.getDefaultToolkit().getScreenSize());
		displayFrame.add(new MazeContainer());
		displayFrame.setVisible(true);
	}
	
	public void init() {
		System.out.println(getParameter("zipfile"));
		
		try {
			MediaSupplier.initilize(this, getParameter("zipfile"));
		} catch (Exception err) {
			err.printStackTrace();
		}
		
		setLayout(new BorderLayout());
		game = new MazeContainer();
		add(game, "Center");
	}	
	
	public String[][] getParameterInfo() {
		String[][] info = {{"zipfile"}, {"The file that contains the archives for Maze Game."}};
		
		return info;
	}
}
