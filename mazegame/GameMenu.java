package mazegame;

import java.awt.*;
import java.awt.event.*;
import java.applet.*;
import java.util.*;
import java.io.*;
import java.net.*;
import java.lang.reflect.Method;
import draff.awt.*;
import draff.awt.event.*;

public class GameMenu extends MenuBar {
	private MazeContainer mazeContainerParent = null;
	public static Properties programInfo;
	
	public GameMenu(MazeContainer mazeContainerParent) {	
		this.mazeContainerParent = mazeContainerParent;
		
		Menu game = new Menu("Game");		
		
		addMissionActivatorMenus(game);
		
		game.addSeparator();

		add(game);

		Menu help = new Menu("Help");

		MenuItem howToPlay = new MenuItem(MazeContainer.programInfo.getString("help.howtoplay.menucaption"));
		howToPlay.addActionListener(new DisplayTextFileActionListener(MazeContainer.programInfo.getString(
				"help.howtoplay.filename"), new Font("Courier", Font.PLAIN, 12)));
		help.add(howToPlay);

		add(help);
	}
	
	private void addMissionActivatorMenus(Menu destinationMenu) {
		try {
			StringTokenizer missionFileNames = 
					new StringTokenizer(MazeContainer.programInfo.getString("missions"), ";", false);
			StringTokenizer missionCaption = 
					new StringTokenizer(MazeContainer.programInfo.getString("missions.captions"), ";", false);
			MenuItem missionActivator;
			while (missionFileNames.hasMoreTokens()) {				
				missionActivator = new MenuItem(missionCaption.nextToken());
				missionActivator.addActionListener(new NewGameActionListener(missionFileNames.nextToken(), mazeContainerParent));
				destinationMenu.add(missionActivator);
			}		
		} catch (Exception err) {
			err.printStackTrace();
		}
	}	

	class NewGameActionListener implements ActionListener, Runnable {
		private String missionFileName;
		private MazeContainer parent;
		private Thread loadGameThread;
		private TextArea errorMessageTextArea;
		private Panel errorMessageDisplay;
		
		public NewGameActionListener(String missionFileName, MazeContainer parent) {
			this.missionFileName = missionFileName;
			this.parent = parent;
			
			errorMessageDisplay = new Panel();
			errorMessageDisplay.setLayout(new BorderLayout());
			Label errorLabel = new Label("Error Loading Game", Label.CENTER);
			errorLabel.setFont(new Font("SanSerif", Font.BOLD, 
					Integer.parseInt(MazeContainer.programInfo.getString("loading.error.labelfontsize"))));
			errorMessageDisplay.add(errorLabel, "North");
			
			errorMessageTextArea = new TextArea();
			errorMessageDisplay.add(errorMessageTextArea, "Center");
			
			parent.addComponentToMainArea(errorMessageDisplay, errorMessageDisplay.getName());
		}
		
		public void actionPerformed(ActionEvent evt) {
			loadGameThread = new Thread(this, "Game loader");
			loadGameThread.setPriority(Thread.MAX_PRIORITY);
			loadGameThread.start();			
		}
		
		public void run() {
			try {
				parent.showLoadingScreen();
				Maze mission = loadMission();
				mazeContainerParent.showGameScreen(mission);
			} catch (Exception err) {
				parent.showComponent(errorMessageDisplay);
				StringWriter messageWriter = new StringWriter();
				err.printStackTrace(new PrintWriter(messageWriter));
				errorMessageTextArea.setText(messageWriter.toString());
			}
		}
		
		private Maze loadMission() throws IOException, ClassNotFoundException {
			Maze mission = (Maze)(new ObjectInputStream(
						MediaSupplier.getInputStream(MazeContainer.programInfo.getString("missions.directory") +
						"/" + missionFileName)).readObject());
			return mission;
		}
	}
}