package mazegame;

import java.util.*;
import java.awt.*;
import java.awt.event.*;
import java.applet.*;
import java.io.*;

public abstract class Maze extends Observable implements Serializable, Cloneable {
	private boolean wasVictoryAchieved;
	private boolean isGameOver = false;
	private Maze previousState = null;
	private Room[] rooms;
	private Player quester;
	private String name;
	
	public Maze() {
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public void setQuester(Player quester) {
		this.quester = quester;
	}
	
	public Player getQuester() {
		return quester;
	}
	
	public Room getRoomAtFloorPlanLocation(Point floorPlanLocation) {					
		for (int roomIndex = 0; roomIndex < getRooms().length; roomIndex++) {
			if (getRooms()[roomIndex].getFloorPlanLocation() != null && 
				getRooms()[roomIndex].getFloorPlanLocation().equals(floorPlanLocation)) {
				
				return getRooms()[roomIndex];				
			}
		}
		
		return null;
	}
	
	public void setRooms(Room[] rooms) {		
		this.rooms = rooms;
	}
	
	public Room[] getRooms() {
		return rooms;
	}
	
	public Vector getRoomsVector() {
		Vector vectorOfRooms = new Vector(getRooms().length);
		
		for (int roomIndex = 0; roomIndex < getRooms().length; roomIndex++) {
			vectorOfRooms.insertElementAt(getRooms()[roomIndex], roomIndex);
		}
		
		return vectorOfRooms;
	}
	
	public void prepareForChange() {
		// Using a clone is necessary because otherwise when the state changes it will change the previous
		// state as well.
		
		try {
			setPreviousState((Maze)clone());
		} catch (CloneNotSupportedException err) {
			err.printStackTrace();
		}
	}
	
	public void update() {		
		setChanged();
		notifyObservers(getPreviousState());
	}
	
	public boolean isGameOver() {
		return isGameOver;
	}
	
	public boolean getWasVictoryAchieved() {
		return wasVictoryAchieved;
	}
	
	public Maze getPreviousState() {
		return previousState;
	}
	
	public void startGame() {
		this.isGameOver = false;
		update();
		
		System.gc();
	}
	
	public void endGame(boolean wasVictoryAchieved) {
		prepareForChange();
		this.wasVictoryAchieved = wasVictoryAchieved;
		this.isGameOver = true;
		update();
		
		System.gc();
	}
	
	public Object clone() throws CloneNotSupportedException {
		Maze clone = (Maze)super.clone();
		
		clone.rooms = (Room[])rooms.clone();
		clone.quester = (Player)quester.clone();
		
		return clone;
	}
	
	private void setPreviousState(Maze previousState) {
		this.previousState = previousState;
	}	
}