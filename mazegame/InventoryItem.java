package mazegame;

import java.awt.*;
import draff.awt.*;

/**
 * An object which the quester can use to interact with the maze.
 * This may be a part of the quester (eg a hand) or an carried object (eg a pen).
 * @author David Raffensperger
 * @version 1.0 20 Aug 1999
 */
public class InventoryItem extends ImageCanvas implements Cloneable {	
	/**
	 * Constructs an InventoryItem.
	 * @param maze The maze in which this belongs.
	 * @param imageFile The file name of the image displayed.
	 * @param displayCursor The mouse cursor that will be displayed when this is selected.
	 */
	public InventoryItem(String imageFile, String name) {
		super (imageFile);

		setName(name);
	}

	public void paint(Graphics g) {
		g.setColor(Color.lightGray);
		g.fill3DRect(0, 0, getSize().width, getSize().height, true);
		
		super.paint(g);
	}
	
	public Object clone() throws CloneNotSupportedException {
		InventoryItem clone = (InventoryItem)super.clone();
		clone.setName(getName());
		clone.setImageFileName(getImageFileName());
		
		return clone;
	}
}
