package mazegame;

import draff.awt.*;
import java.awt.*;
import java.awt.event.*;
import java.util.*;

public class Room extends MazeViewerPanel implements Cloneable {
	private Color programBackground = null;
	private ImageCanvas bgDisplayer;
	private Vector itemsInRoom;
	private String bgDisplayerImageFile;
	private static int roomThatExist = 0;
	private Point floorPlanLocation;
	
	// It's necessary to make itemsPanel an ItemsDisplayer instead of just a Panel,
	// because instances of Panel are not light weight, but ItemsDisplayer is.
	private LightWeightPanel itemsPanel;

	public Room(String imageFile, Color programBackground) {
		this (null, imageFile, programBackground);
	}
	
	public Room(Maze maze, String imageFile, Color programBackground) {
		super (maze);

		setProgramBackground(programBackground);
		
		bgDisplayerImageFile = imageFile;
		setLayout(new AllComponentsMaxSizeLayout());
		setName(getClass().getName() + roomThatExist);
		roomThatExist++;
		
		itemsInRoom = new Vector();
	}
		
	public void setMaze(Maze maze) {
		super.setMaze(maze);

		itemsPanel = new LightWeightPanel();
		bgDisplayer = new ImageCanvas(bgDisplayerImageFile);
		itemsPanel.setLayout(new ScaleLayout(bgDisplayer.getPreferredSize()));		
		
		add(itemsPanel);		
		add(bgDisplayer);
	}
	
	public Color getProgramBackground() {
		return programBackground;
	}
	
	public void setProgramBackground(Color background) {
		this.programBackground = background;
	}
	
	public Vector getItemsInRoom() {
		return itemsInRoom;
	}
	
	public void setItemsInRoom(Vector itemsInRoom) {
		this.itemsInRoom = itemsInRoom;
	}

	public void addRoomItem(RoomItem item) {
		/*GridBagConstraints itemConstraints = new GridBagConstraints();
		
		//itemConstraints.gridheight = item.getPreferredSize().height / 50;
		//itemConstraints.gridwidth = item.getPreferredSize().width / 50;
		itemConstraints.gridx = item.getLocation().x;
		itemConstraints.gridy = item.getLocation().y;
		itemConstraints.weightx = item.getPreferredSize().width;
		itemConstraints.weighty = item.getPreferredSize().height;
		itemConstraints.fill = GridBagConstraints.BOTH;
		
		((GridBagLayout)itemsPanel.getLayout()).setConstraints(item, itemConstraints);*/
		
		itemsPanel.add(item);		
		itemsInRoom.addElement(item);
	}

	public void removeRoomItem(RoomItem item) {
		itemsInRoom.removeElement(item);
		itemsPanel.remove(item);
		itemsPanel.validate();
		itemsPanel.repaint();
	}

	public void mazeStateChanged(Maze previousState) {
		
	}
	
	public void setFloorPlanLocation(Point floorPlanLocation) {
		this.floorPlanLocation = floorPlanLocation;
	}
	
	public Point getFloorPlanLocation() {
		return floorPlanLocation;
	}
	
	public Object clone() throws CloneNotSupportedException {
		Room clone = (Room)super.clone();
		
		clone.programBackground = new Color(programBackground.getRGB());
		clone.itemsInRoom = (Vector)itemsInRoom.clone();
		
		return clone;
	}
}