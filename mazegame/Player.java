package mazegame;

import java.util.Vector;
import java.io.Serializable;
import java.util.*;

public class Player implements Serializable, Cloneable {
	private Hashtable inventory;
	private int roomLocatedIn = 0;
	private Maze maze;

	public Player(Maze maze) {
		this.maze = maze;
		
		inventory = new Hashtable();
		roomLocatedIn = getStartingRoom();
	}

	public Maze getMaze() {
		return maze;
	}
	
	public int getRoomLocatedIn() {
		return roomLocatedIn;
	}

	public void setRoomLocatedIn(int roomLocatedIn) {
		this.roomLocatedIn = roomLocatedIn;
	}

	public void addInventoryItem(InventoryItem item) {
		inventory.put(item.getName(), item);
	}

	public void removeInventoryItem(InventoryItem item) {
		inventory.remove(item.getName());
	}

	public Enumeration getInventory() {
		return inventory.elements();
	}
	
	public boolean has(String itemType) {
		return inventory.containsKey(itemType);
	}
	
	public int getStartingRoom() {
		return 0;
	}
	
	public void won() {
		getMaze().endGame(true);
	}
	
	public void lost() {
		getMaze().endGame(false);
	}
	
	public Object clone() throws CloneNotSupportedException {
		Player clone = (Player)super.clone();		
		clone.inventory = (Hashtable)inventory.clone();
		return clone;
	}
}