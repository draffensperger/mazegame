package draffutil;

import java.io.*;

public class TrimBlankStartLines {
	public static void main(String[] args) {
		int firstNonBlankLineNumber = 0;
		String fileToTrim;
		
		try {
			fileToTrim = args[0];
		} catch (ArrayIndexOutOfBoundsException noArgumentsPassed) {
			System.out.println("Syntax: <JVM> draffutil.TrimBlankStartLines <file to trim>");
			return;
		}

		try {
			LineNumberReader in = new LineNumberReader(new FileReader(fileToTrim));
			String currentLine = in.readLine();
			
			try {
				while (currentLine.equals("")) {
					firstNonBlankLineNumber++;
					currentLine = in.readLine();
				}
			} catch (NullPointerException firstLineInNull) {
				System.out.println("The file " + fileToTrim + " has no non-blank lines.");
				return;
			}
			
			StringBuffer data = new StringBuffer();
			while (currentLine != null) {
				data.append(currentLine + "\n");
				currentLine = in.readLine();
			}
	
			in.close();
			
			FileWriter out = new FileWriter(fileToTrim);
			out.write(data.toString());
			out.flush();
			out.close();
		} catch (IOException err) {
			err.printStackTrace();
		}
		
		System.out.println(firstNonBlankLineNumber + " starting blank lines trimmed from " + fileToTrim);
	}
}
