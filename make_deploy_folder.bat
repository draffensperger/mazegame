jar -cf mazegame.jar draff/awt/*.class draff/awt/event/*.class draff/io/*.class draff/lang/reflect/*.class mazegame/*.class mazecomponents/*.class imagedisplayer/*.class mazegenerator/*.class
rmdir /Q /S _deploy
mkdir _deploy
cd _deploy
mkdir missions
mkdir images
mkdir help
cd ..
copy mazegame.properties _deploy
copy mazegame_jar.html _deploy
copy mazegame.jar _deploy
xcopy /Y help _deploy\help
xcopy /Y images _deploy\images
xcopy /Y missions\*.obj _deploy\missions